<div class="admin sitemap page">
	<h1><?php echo _($entry->getId() ? 'Edit' : 'Create'); ?></h1>
	<?php if (\forge\Controller::getCode() == \forge\Controller::RESULT_BAD): ?>
		<p class="error"><?php echo self::html(\forge\Controller::getMessage()); ?></p>
	<?php endif; ?>
	<form action="/admin/SiteMap/page" method="post" name="page">
		<input type="hidden" name="forge[controller]" value="SiteMap\Set" />
		<?php echo self::input('hidden', 'page[id]', $entry->getId()); ?>
		<h2><?php echo _('Settings'); ?></h2>
		<table>
			<tr>
				<td><?php echo _('Title'); ?>:</td>
				<td><?php echo self::input('text', 'page[title]', $entry->page_title, true, ['id' => 'page-title', 'onchange' => 'mkuri();']); ?></td>
			</tr>
			<tr>
				<td><?php echo _('Type'); ?>:</td>
				<td>
					<select name="page[type]" onchange="display(this.options[this.selectedIndex].value.replace(/\\/g, '_'));">
						<option></option>
						<?php foreach ($types as $index => $type): ?>
							<option value="<?php echo self::html($type->getName()); ?>" id="type<?php echo $index; ?>"<?php if ($entry->page_type == $type->getName()): ?> selected="selected"<?php elseif ($entry->page_type): ?> disabled="disabled"<?php endif; ?>><?php echo self::html($type->getTitle()); ?></option>
						<?php endforeach; ?>
					</select>
				</td>
			</tr>
		</table>
		<h2><?php echo _('Hierarchy'); ?></h2>
		<table>
			<tr>
				<td><?php echo _('Parent'); ?>:</td>
				<td>
					<select name="page[parent]" onchange="determineUri();">
						<option value="0"></option>
						<?php foreach ($pages as $page): ?>
							<option value="<?php echo $page->getId(); ?>" title="<?php echo $page->page_url; ?>" value="<?php echo $page->getId(); ?>"<?php if($entry->page_parent == $page->getId()): ?> selected="selected"<?php endif; ?>><?php echo $page->page_title; ?></option>
						<?php endforeach; ?>
					</select>
				</td>
			</tr>
			<tr>
				<td><?php echo _('URL'); ?>:</td>
				<td><?php echo self::input('text', 'page[url]', $entry->page_url, true, ['id' => 'page-url']); ?></td>
			</tr>
			<tr>
				<td><?php echo _('Publish'); ?>:</td>
				<td><?php echo self::input('checkbox', 'page[publish]', 1, $entry->page_publish ? ['checked'=>'checked'] : array()); ?></td>
			</tr>
			<tr>
				<td><?php echo _('Menu'); ?>:</td>
				<td><?php echo self::input('checkbox', 'page[menu]', 1, $entry->page_menu ? ['checked'=>'checked'] : array()); ?></td>
			</tr>
			<tr>
				<td><?php echo _('Default'); ?>:</td>
				<td><?php echo self::input('checkbox', 'page[default]', 1, $entry->page_default ? ['checked'=>'checked'] : array()); ?></td>
			</tr>
		</table>
		<h2><?php echo _('Meta data'); ?></h2>
		<table>
			<tr>
				<td><?php echo _('Description'); ?>:</td>
				<td><?php echo self::input('text', 'page[meta_description]', $entry->meta_description); ?></td>
			</tr>
			<tr>
				<td><?php echo _('Keywords'); ?>:</td>
				<td><?php echo self::input('text', 'page[meta_keywords]', $entry->meta_keywords); ?></td>
			</tr>
		</table>
		<?php if (!$entry->getId()): ?>
			<?php foreach ($types as $index => $type): ?>
			<div id="<?php echo str_replace('\\','_',$type->getName()); ?>" class="plugin-form">
				<?php echo $type->getCreationForm(); ?>
			</div>
			<?php endforeach; ?>
		<?php else: ?>
			<?php echo $instance->getEditForm($entry->getId()); ?>
		<?php endif; ?>
		<p><input type="submit" value="<?php echo _($entry->getId() ? _('Save') : _('Create')); ?>"></p>
	</form>
</div>