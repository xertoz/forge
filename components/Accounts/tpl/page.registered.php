<div class="accounts registered">
	<h1><?php echo _('Thank You!'); ?></h1>
	<p><?php echo _('Your account has been registered. You must active it by clicking the link that will be sent to your email in a few moments.'); ?></p>
</div>