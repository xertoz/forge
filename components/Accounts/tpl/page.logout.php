<div class="accounts logout">
	<h1><?php echo _('Log out'); ?></h1>
	<p><?php echo _('You have successfully been logged out.'); ?></p>
	<p><?php echo _('It is now possible to close the browser or continue surfing <a href="/">this site</a> as a guest.'); ?></p>
</div>