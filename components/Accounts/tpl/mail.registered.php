<h1><?php echo _('Hi %name%!'); ?></h1>
<p><?php echo _('We would like to give you a warm welcome to our community.'); ?></p>
<p><?php echo _('The account that you have signed up for with us, enables you to login on the following websites:'); ?></p>
<p>%sites%</p>
<p><?php echo _('But before you can use your account, you need to confirm your account. Please use the following link to activate your account:'); ?></p>
<p>%link%</p>
<p><?php echo _('If you haven&#39;t signed up on our community, you should e-mail one of our support adresses. You can find them on our websites listed above.'); ?></p>
<p><i><?php echo _('This was an automated message - you can\'t reply to it.'); ?></i></p>